import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'pruebaSFP';
  estadoFormulario= "CDMX";
  cpFormulario = "14040";
  get_Estados;
  get_CP_Por_Estado;
  get_Municipio_Por_Estado;
  get_Colonia_Por_CP;

  ngOnInit() {
    
    let endpoint_sepomex = "http://api-sepomex.hckdrk.mx/query/";
    let method_sepomex = 'info_cp/';
    let cp = "09810";
    let variable_string = '?type=simplified';
    let urlgetEstado = endpoint_sepomex+'get_estados'  //endpoint_sepomex + method_sepomex + cp + variable_string;

    this.getEstado(urlgetEstado);
    
    let urlgetCP = endpoint_sepomex+'get_cp_por_estado/'+'Ciudad de Mexico'
    this.getCP(urlgetCP);

    let urlgetMunicipio = endpoint_sepomex+'get_municipio_por_estado/'+'Ciudad de Mexico'
    this.getMunicipio(urlgetMunicipio);

    let urlgetColonia = endpoint_sepomex+'get_colonia_por_cp/'+'01000'
    this.getColonia(urlgetColonia);
  }

  getEstado(url){
    let request = new XMLHttpRequest();
    request.open('GET', url);
    request.send();
    request.onreadystatechange = () => {
      if (request.readyState === 4) {
        console.log(JSON.parse(request.response).response.estado);
        this.get_Estados = JSON.parse(request.response).response.estado;
        //console.log(get_Estados)
      }
    }
  }
  getCP(url){
    let request = new XMLHttpRequest();
    request.open('GET', url);
    request.send();
    request.onreadystatechange = () => {
      if (request.readyState === 4) {
        console.log(JSON.parse(request.response).response.cp);
        this.get_CP_Por_Estado = JSON.parse(request.response).response.cp;
        //console.log(get_Estados)
      }
    }
  }
  getMunicipio(url){
    let request = new XMLHttpRequest();
    request.open('GET', url);
    request.send();
    request.onreadystatechange = () => {
      if (request.readyState === 4) {
        console.log(JSON.parse(request.response).response.municipios);
        this.get_Municipio_Por_Estado = JSON.parse(request.response).response.municipios;
        //console.log(get_Estados)
      }
    }
  }
  getColonia(url){
    let request = new XMLHttpRequest();
    request.open('GET', url);
    request.send();
    request.onreadystatechange = () => {
      if (request.readyState === 4) {
        console.log(JSON.parse(request.response).response.colonia);
        this.get_Colonia_Por_CP = JSON.parse(request.response).response.colonia;
        //console.log(get_Estados)
      }
    }
  }
}


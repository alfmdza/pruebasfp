import { Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.scss']
})
export class FormularioComponent implements OnInit {


  constructor() { }

  get_Estados;
  get_CP_Por_Estado;
  get_Municipio_Por_CP;
  get_Colonia_Por_CP;

  endpoint_sepomex = "http://api-sepomex.hckdrk.mx/query/";
  ngOnInit() {

    let urlgetEstado = this.endpoint_sepomex + 'get_estados';
    this.getEstado(urlgetEstado);

  }

  getEstado(url) {
    let request = new XMLHttpRequest();
    request.open('GET', url);
    request.send();
    request.onreadystatechange = () => {
      if (request.readyState === 4) {
        console.log(JSON.parse(request.response).response.estado);
        this.get_Estados = JSON.parse(request.response).response.estado;

        let urlgetCP = this.endpoint_sepomex + 'get_cp_por_estado/' + this.get_Estados[0]
        this.getCP(urlgetCP);


      }
    }
  }
  getCP(url) {
    let request = new XMLHttpRequest();
    request.open('GET', url);
    request.send();
    request.onreadystatechange = () => {
      if (request.readyState === 4) {
        console.log(JSON.parse(request.response).response.cp);
        this.get_CP_Por_Estado = JSON.parse(request.response).response.cp;
        let urlgetInfoCP = this.endpoint_sepomex + 'info_cp/' + this.get_CP_Por_Estado[0] + '?type=simplified'
        this.getInfoCP(urlgetInfoCP)
      }
    }
  }

  getInfoCP(url) {
    let request = new XMLHttpRequest();
    request.open('GET', url);
    request.send();
    request.onreadystatechange = () => {
      if (request.readyState === 4) {
        this.get_Municipio_Por_CP = [JSON.parse(request.response).response.municipio];
        this.get_Colonia_Por_CP = JSON.parse(request.response).response.asentamiento;
      }
    }
  }

  onChangeEstado(new_estado) {
    let urlgetCP = this.endpoint_sepomex + 'get_cp_por_estado/' + new_estado
    this.getCP(urlgetCP);
  }
  onChangeCP(new_cp) {
    let urlgetInfoCP = this.endpoint_sepomex + 'info_cp/' + new_cp + '?type=simplified'
        this.getInfoCP(urlgetInfoCP)
  }

}
